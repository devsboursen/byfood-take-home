import create from 'zustand';
import { devtools } from 'zustand/middleware';
interface SelectionState {
  selection: string;
  change: (selection: string) => void;
}

const useSelectionStore = create<SelectionState>()(
  devtools((set) => ({
    selection: '',
    change: (selection) => set({ selection }),
  })),
);

export default useSelectionStore;
