export type User = {
  ID: string;
  name: string;
  email: string;
  phone: string;
  country: string;
}
