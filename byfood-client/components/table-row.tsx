import { User } from '../types';
import { ReactElement } from 'react';

interface TableRowProps {
  user: User;
  selected: boolean;
  onClick: (e: any) => void;
}

const TableRow = ({ user, onClick, selected }: TableRowProps) => {
  return (
    <tr
      onClick={onClick}
      className={` table-row mb-10 lg:mb-0 cursor-pointer group ${
        selected ? 'bg-primary hover:bg-red-500' : 'bg-white hover:bg-secondary'
      }`}
    >
      <td className="w-auto p-3 text-gray-800 text-center border border-b table-cell static">
        {user.name}
      </td>
      <td className="w-auto p-3 text-gray-800 text-center border border-b table-cell static">
        {user.email}
      </td>
      <td className="w-auto p-3 text-gray-800 text-center border border-b table-cell static">
        {user.country}
      </td>
      <td className="w-auto p-3 text-gray-800 text-center border border-b table-cell static">
        {user.phone}
      </td>
    </tr>
  );
};

export default TableRow;
