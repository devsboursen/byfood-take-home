import { ReactElement } from 'react';
import { useFormik } from 'formik';
import { User } from '../types';
import FormInput from './form-input';
import Button from './button';
import { FaRegSave } from 'react-icons/fa';
import { AiOutlineRollback } from 'react-icons/ai';
import { useRouter } from 'next/router';
import { PostData } from '../pages/new/index';

interface FormProps {
  formType: 'New' | 'Edit' | 'Delete';
  user: User;
  onSubmit: (data?: PostData) => Promise<Response>;
}

const buttonNameMapping: { [key: string]: string } = {
  New: 'Create',
  Edit: 'Save',
  Delete: 'Delete',
};

const UserForm = ({ formType, user, onSubmit }: FormProps): ReactElement => {
  const router = useRouter();

  const formik = useFormik({
    initialValues: {
      name: user.name,
      email: user.email,
      phone: user.phone,
      country: user.country,
    },

    onSubmit: async (values) => {
      const response = await onSubmit(values);

      if (formType === 'New' && response.status !== 201) {
        console.log('failed to Create User ', response.status);
      }

      if (response.status !== 200) {
        console.log('failed to update or delete User ', response.status);
      }

      router.push('/');
    },
  });

  return (
    <form
      className=" flex flex-col gap-3 max-w-2xl w-screen p-4 md:p-12 bg-white rounded-2xl shadow-lg"
      onSubmit={formik.handleSubmit}
    >
      <h1 className=" pl-3 font-sans text-3xl ">{`${formType} User`}</h1>

      <FormInput
        label="Name"
        name="name"
        type="text"
        placeholder="Jane Doe"
        value={formik.values.name}
        onChange={formik.handleChange}
      />

      <FormInput
        label="Email"
        name="email"
        type="email"
        placeholder="Jane.Doe@nomail.com"
        value={formik.values.email}
        onChange={formik.handleChange}
      />

      <FormInput
        label="Country"
        name="country"
        type="text"
        placeholder="Morocco"
        value={formik.values.country}
        onChange={formik.handleChange}
      />

      <FormInput
        label="Phone number"
        name="phone"
        type="tel"
        placeholder="000 - 000 000 000"
        value={formik.values.phone}
        onChange={formik.handleChange}
      />

      <div className="flex flex-row justify-between items-center p-4">
        <Button
          icon={<AiOutlineRollback size={'1.5rem'} />}
          onClick={() => {
            router.back();
          }}
          title="Go Back"
        ></Button>
        <Button
          icon={<FaRegSave size={'1.5rem'} />}
          onClick={() => {}}
          title={`${buttonNameMapping[formType]}`}
          buttonType="submit"
          active
        ></Button>
      </div>
    </form>
  );
};

export default UserForm;
