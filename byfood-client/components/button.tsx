import { ReactElement } from 'react';

interface ButtonProps {
  icon: ReactElement;
  title: string;
  buttonType?: 'submit' | 'button';
  active?: boolean;
  onClick: (e: any, userId?: string) => void;
  disabled?: boolean;
}

const Button = ({
  icon,
  title,
  onClick,
  disabled,
  active = false,
  buttonType = 'button',
}: ButtonProps): ReactElement => {
  return (
    <button
      type={buttonType}
      className={`flex flex-col cursor-pointer items-center justify-center w-20 h-20 text-sm font-semibold transition-colors rounded-xl
     ${
       active
         ? 'bg-primary bg-gradient-to-r from-red-400 to-primary text-white'
         : ''
     }
      ${
        !disabled
          ? active
            ? 'text-gray-100 hover:bg-gray-100 hover:bg-gradient-to-r hover:from-secondary hover:to-secondary hover:text-primary'
            : 'text-gray-600 bg-gray-100 hover:text-white hover:bg-gradient-to-r hover:bg-primary hover:from-red-400 hover:to-primary'
          : 'text-gray-400 cursor-not-allowed'
      }
      `}
      onClick={onClick}
      disabled={!!disabled}
    >
      <span className="mb-2">{icon}</span>
      {title}
    </button>
  );
};

export default Button;
