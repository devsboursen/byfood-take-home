import Image from 'next/image';
import { useRouter } from 'next/router';
import Button from './button';
import { FaUserPlus, FaUserEdit, FaUserSlash } from 'react-icons/fa';
import { ReactElement } from 'react';
import useSelectionStore from '../store/store';

const Header = (): ReactElement => {
  const selected = useSelectionStore((state) => state.selection);
  const router = useRouter();

  const handleNew = () => {
    router.push('/new');
  };

  const handleEdit = (e: any, userId: string): void => {
    router.push({
      pathname: '/edit/[id]',
      query: { id: userId },
    });
  };

  const handleDelete = (e: any, userId: string): void => {
    router.push({
      pathname: '/delete/[id]',
      query: { id: userId },
    });
  };

  return (
    <header className="fixed top-0 z-10 flex w-full flex-row justify-between items-center p-2 shadow-md bg-white bg-opacity-90 backdrop-blur-sm ">
      <Image
        priority
        src={'/byfood.png'}
        width={100}
        height={100}
        alt="logo"
      ></Image>

      <div className="flex flex-row justify-center gap-3 items-center">
        <Button
          icon={<FaUserPlus size={'1.5rem'} />}
          onClick={handleNew}
          title={'New'}
        />
        <Button
          icon={<FaUserEdit size={'1.5rem'} />}
          onClick={(e) => handleEdit(e, selected)}
          title={'Edit'}
          disabled={selected === ''}
        />
        <Button
          icon={<FaUserSlash size={'1.5rem'} />}
          onClick={(e) => handleDelete(e, selected)}
          title={'Delete'}
          disabled={selected === ''}
        />
      </div>
    </header>
  );
};

export default Header;
