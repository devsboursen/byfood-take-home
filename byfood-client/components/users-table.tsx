import { User } from '../types';
import { ReactElement, useEffect, useState } from 'react';
import { FaInfoCircle } from 'react-icons/fa';
import useSelectionStore from '../store/store';
import TableRow from './table-row';

const UsersTable = ({ users }: { users: User[] }): ReactElement => {
  const [selectedRow, setSelectedRow] = useState<{ [key: string]: boolean }>(
    {},
  );
  const selected = useSelectionStore((state) => state.selection);
  const changeSelected = useSelectionStore((state) => state.change);

  useEffect(() => {
    if (users.length === 0) return;
    setSelectedRow(
      users
        .map((user) => user['ID'])
        .reduce<{ [key: string]: boolean }>((newState, userId) => {
          newState[userId] = false;
          return newState;
        }, {}),
    );
  }, []);

  const handleTableRowClick = (e: any, rowId: string): void => {
    if (e === null) {
      return;
    }
    setSelectedRow((prevState) => {
      const newState = { ...prevState };
      if (selected !== '') {
        newState[selected] = false;
      }
      newState[rowId] = true;
      return newState;
    });
    changeSelected(rowId);
  };

  return (
    <main className="relative top-24 p-12 lg:p-24 w-full bg-white">
      <table className="border-collapse w-full">
        <thead>
          <tr>
            <th className="p-3 font-bold uppercase bg-secondary text-gray-600 border border-gray-300 table-cell">
              Full Name
            </th>
            <th className="p-3 font-bold uppercase bg-secondary text-gray-600 border border-gray-300 table-cell">
              Email
            </th>
            <th className="p-3 font-bold uppercase bg-secondary text-gray-600 border border-gray-300 table-cell">
              Country
            </th>
            <th className="p-3 font-bold uppercase bg-secondary text-gray-600 border border-gray-300 table-cell">
              Phone Number
            </th>
          </tr>
        </thead>
        <tbody>
          {users.length > 0 &&
            users.map((user) => (
              <TableRow
                key={user['ID']}
                onClick={(e) => handleTableRowClick(e, user['ID'])}
                user={user}
                selected={selectedRow[user['ID']]}
              ></TableRow>
            ))}
        </tbody>
      </table>
      {users.length === 0 && (
        <div className="flex flex-row gap-5 w-auto text-primary bg-secondary rounded-lg span items-center justify-center p-12 m-2">
          <FaInfoCircle size={'2rem'} />
          <span>No registered Users!</span>
        </div>
      )}
    </main>
  );
};

export default UsersTable;
