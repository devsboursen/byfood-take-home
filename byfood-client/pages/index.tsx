import Head from 'next/head';
import Header from '../components/header';
import UsersTable from '../components/users-table';
import { User } from '../types';
import { GetServerSideProps } from 'next';
import useSelectionStore from '../store/store';

export default function Home({ users }: { users: User[] }) {
  const changeSelected = useSelectionStore((state) => state.change);
  changeSelected('');

  return (
    <>
      <Head>
        <title>byFood Community</title>
        <meta
          name="description"
          content="A list of individuals who like what byFood offers"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Header />

      <UsersTable users={users} />
    </>
  );
}

export const getServerSideProps: GetServerSideProps<{
  users: User[];
}> = async () => {
  const res = await fetch(process.env.API_ENDPOINT || 'http://localhost:8080');

  if (res.status !== 200) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    };
  }

  const users: User[] = await res.json();

  return { props: { users } };
};
