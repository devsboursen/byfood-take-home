import Head from 'next/head';
import { GetServerSideProps, NextPage } from 'next';
import UserForm from '../../components/user-form';
import { User } from '../../types';
import useSelectionStore from '../../store/store';

export interface PostData {
  ID?: string;
  name: string;
  email: string;
  phone: string;
  country: string;
}

const EditUser: NextPage<{ user: User }> = ({ user }) => {
  const handleCreate = async (data?: PostData) => {
    const url = `${process.env.API_ENDPOINT || 'http://localhost:8080'}/users/${
      user['ID']
    }`;

    const response = await fetch(url, {
      method: 'put',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify(data),
    });

    return response;
  };

  return (
    <main className="h-screen flex flex-col justify-center w-full items-center">
      <UserForm formType="Edit" user={user} onSubmit={handleCreate} />
    </main>
  );
};

export default EditUser;

export const getServerSideProps: GetServerSideProps<{
  user: User;
}> = async (context) => {
  const id = context?.params?.id;

  if (id === undefined || typeof id === 'object') {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    };
  }

  const url = `${
    process.env.API_ENDPOINT || 'http://localhost:8080'
  }/users/${id}`;

  const res = await fetch(url);

  if (res.status !== 200) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    };
  }

  const user: User = await res.json();

  return { props: { user } };
};
