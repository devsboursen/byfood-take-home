import Head from 'next/head';
import { NextPage } from 'next';
import UserForm from '../../components/user-form';
import { User } from '../../types';
import useSelectionStore from '../../store/store';

export interface PostData {
  ID?: string;
  name: string;
  email: string;
  phone: string;
  country: string;
}

const NewUser: NextPage = () => {
  const user: User = {
    ID: '',
    name: '',
    email: '',
    phone: '',
    country: '',
  };

  const handleCreate = async (data?: PostData) => {
    const url = `${process.env.API_ENDPOINT || 'http://localhost:8080'}/users`;
    console.log(url);
    const response = await fetch(url, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify(data),
    });

    return response;
  };

  return (
    <>
      <Head>
        <title>byFood Community</title>
        <meta name="description" content="Create User" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="h-screen flex flex-col justify-center w-full items-center">
        <UserForm formType="New" user={user} onSubmit={handleCreate} />
      </main>
    </>
  );
};

export default NewUser;
