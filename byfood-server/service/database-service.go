package service

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/devsboursen/byfood-take-home/byfood-server/model"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

func DatabaseConnector() (*gorm.DB, error) {
	var envs map[string]string
	envs, err := godotenv.Read(".env")

	if err != nil {
		log.Fatal("Error loading database credentials from .env file")
	}

	hostname := envs["DB_HOST"]
	username := envs["DB_USER"]
	password := envs["DB_PASSWORD"]
	dbname := envs["DB_NAME"]
	port := envs["DB_PORT"]

	dsn := fmt.Sprintf("host=%s user=%s password=%s database=%s port=%s", hostname, username, password, dbname, port)

	sqlDB, _ := sql.Open("pgx", dsn)
	db, err := gorm.Open(postgres.New(postgres.Config{
		Conn: sqlDB,
	}), &gorm.Config{})

	if err != nil {
		log.Fatal(err.Error())
	}

	return db, err

}

func databaseMigrator() (*gorm.DB, error) {
	db, err := DatabaseConnector()

	if err != nil {
		log.Println(err)
		log.Println("Could not connect to database!")
		return db, err
	}

	if err := db.AutoMigrate(&model.User{}); err != nil {
		log.Println(err)
	}

	return db, err
}

func DatabaseSetter() {
	db, err := databaseMigrator()

	if err != nil {
		log.Println(err)
		log.Println("Could not run migrations!")
	}

	if db.Migrator().HasTable(&model.User{}) {
		var rowCount int64
		db.Table("users").Count(&rowCount)

		if rowCount == 0 {
			data, err := os.ReadFile("./seed/people.json")
			if err == nil {
				var users []model.User
				err := json.Unmarshal(data, &users)

				if err == nil {
					db.Model(&model.User{}).Create(users)
				}
			}
		}
	}

	DB = db
}
