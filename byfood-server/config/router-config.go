package config

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/devsboursen/byfood-take-home/byfood-server/controller"
)

func Router() *gin.Engine {
	router := gin.Default()

	router.Use(cors.Default())

	router.GET("/", controller.GetUsers)
	router.GET("/users", controller.GetUsers)
	router.GET("/users/:id", controller.GetUserByID)
	router.POST("/users", controller.CreateUser)
	router.PUT("/users/:id", controller.UpdateUser)
	router.DELETE("/users/:id", controller.DeleteUser)

	return router
}
