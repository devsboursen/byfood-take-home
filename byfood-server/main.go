package main

import (
	"gitlab.com/devsboursen/byfood-take-home/byfood-server/config"
	"gitlab.com/devsboursen/byfood-take-home/byfood-server/service"
)

func main() {
	service.DatabaseSetter()

	config.Router().Run(":8080")
}
