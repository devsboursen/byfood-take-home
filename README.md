<a name="readme-top"></a>

<!-- TABLE OF CONTENTS -->

# 📗 Table of Contents

- [📗 Table of Contents](#-table-of-contents)
- [📖 byfood-take-home ](#-byfood-take-home-)
  - [🛠 Built With ](#-built-with-)
    - [Tech Stack ](#tech-stack-)
    - [Task Completion ](#task-completion-)
  - [🚀 Video Demo ](#-video-demo-)
  - [💻 Getting Started ](#-getting-started-)
    - [Prerequisites](#prerequisites)
    - [Setup and Usage](#setup-and-usage)
      - [Server side](#server-side)
      - [Client side](#client-side)
  - [👥 Authors ](#-authors-)
  - [🔭 Future Features ](#-future-features-)
  - [🤝 Contributing ](#-contributing-)
  - [⭐️ Show your support ](#️-show-your-support-)
  - [📝 License ](#-license-)

<!-- PROJECT DESCRIPTION -->

# 📖 byfood-take-home <a name="about-project"></a>

> **byfood-take-home** is my complete solution for **byFood**'s technical assessment.

## 🛠 Built With <a name="built-with"></a>

### Tech Stack <a name="tech-stack"></a>

> The technical assessment contains four tasks in total, to be completed using Go as a scripting/back-end language and TypeScript as a front-end language

<details>
  <summary>Client</summary>
  <ul>
    <li><a href="https://tailwindcss.com/">TailwindCSS</a></li>
    <li><a href="https://axios-http.com/">Axios</a></li>
    <li><a href="https://www.typescriptlang.org/">TypeScript</a></li>
    <li><a href="https://reactjs.org/">React.js</a></li>
    <li><a href="https://nextjs.org/">Next.js</a></li>
  </ul>
</details>

<details>
  <summary>Server</summary>
  <ul>
    <li><a href="https://go.dev/">Go</a></li>
    <li><a href="https://gin-gonic.com/">Gin web framework</a></li>
  </ul>
</details>

<details>
<summary>Database</summary>
  <ul>
    <li><a href="https://www.postgresql.org/">PostgreSQL</a></li>
  </ul>
</details>

<!-- Features -->

### Task Completion <a name="task-completion"></a>

- [x] Task 1: DSA problem 1
- [x] Task 2: DSA problem 2
- [x] Task 3: DSA problem 3
- [x] Task 4: Full-Stack web project

<p align="right"><a href="#readme-top">⬆️</a></p>

<!-- LIVE DEMO -->

## 🚀 Video Demo <a name="live-demo"></a>

  [Demo Link](https://www.loom.com/share/b2ae52ed60814717ae73ec89e33f1f88)

<p align="right"><a href="#readme-top">⬆️</a></p>

<!-- GETTING STARTED -->

## 💻 Getting Started <a name="getting-started"></a>

> Description

### Prerequisites

In order to run this project you need:

- Modern browser (Firefox or Chrome preferred)
- Running PostgreSQL
- Go programming language version 1.19.x
- Node 16.14.x
- npm 8.13.x

### Setup and Usage

Clone this repository to your desired folder:

```sh
  cd my-folder
  git clone git@gitlab.com:devsboursen/byfood-take-home.git
```

#### Server side

1. open a new terminal an cd into the `byfood-server` directory:
   ```sh
   cd byfood-server
   go mod vendor
   ```

2) create a `.env` file in the `byfood-server` with the following structure:

   ```sh
   DB_HOST="e.g. localhost"
   DB_USER="your db superuser/owner"
   DB_NAME="your db name"
   DB_PASSWORD="db owner password"
   DB_PORT="5432"
   ```

3) run the server using
   ```sh
   go run main.go
   ```

#### Client side

1. open a new terminal an cd into the `byfood-client` directory:
   ```sh
   cd byfood-client
   npm install
   ```

2) create a `.env.local` file in the `byfood-client` holding the server url (the default value in example) with the following structure:
   ```sh
   API_ENDPOINT="http://localhost:8080"
   ```
3) run the client live server using
   ```sh
   npm run dev
   ```
4) open the url in the browser. default: `http://localhost:3000/`

<p align="right"><a href="#readme-top">⬆️</a></p>

<!-- AUTHORS -->

## 👥 Authors <a name="authors"></a>

> Mention all of the collaborators of this project.

👤 **Soufiane Boursen**

- GitHub: [@Sbouren](https://github.com/Sboursen)
- Twitter: [@sboursen_dev](https://twitter.com/sboursen_dev)
- LinkedIn: [@sboursen](https://linkedin.com/in/sboursen)

<p align="right"><a href="#readme-top">⬆️</a></p>

<!-- FUTURE FEATURES -->

## 🔭 Future Features <a name="future-features"></a>

> Describe 1 - 3 features you will add to the project.

- [x] **Client-side form validation**
- [x] **Client-side Data fetching refactoring**
- [x] **Unit and feature testing**
- [x] **Deployment**
- [x] **CI/CD workflows**
- [x] [**Fixing Issues**](https://gitlab.com/devsboursen/byfood-take-home/-/issues)

<p align="right"><a href="#readme-top">⬆️</a></p>

<!-- CONTRIBUTING -->

## 🤝 Contributing <a name="contributing"></a>

Contributions, issues, and feature requests are welcome!

Feel free to check the [issues page](<[../../issues/](https://gitlab.com/devsboursen/byfood-take-home/-/issues)>).

<p align="right"><a href="#readme-top">⬆️</a></p>

<!-- SUPPORT -->

## ⭐️ Show your support <a name="support"></a>

If you like this project give a star

<p align="right"><a href="#readme-top">⬆️</a></p>

## 📝 License <a name="license"></a>

This project is [MIT](./LICENSE) licensed.

<p align="right"><a href="#readme-top">⬆️</a></p>
