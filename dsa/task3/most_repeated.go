package task3

// MostRepeated returns the most repeated word in the given string slice.
// If two or more words have the same count, it returns the one found closest to the end of the slice.
func MostRepeated(words []string) string {
	if len(words) == 0 {
		return ""
	}
	
	m := make(map[string]int)
	maxCount := 0
	word := words[0]

	for _, w := range words {
		m[w] += 1
		if maxCount <= m[w] {
			maxCount = m[w]
			word = w
		}
	}

	return word
}
