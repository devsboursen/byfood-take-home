# Task 3

## Task description:

> Write a function which takes one parameter as an array/list.
Find most repeated data within a given array.

## Examples:

- **Example 1**:
  - Input :
      ```
      ["apple","pie","apple","red","red","red"]
      ```
  - Output :
      ```
      "red"
      ```
- **Example 2**:
  - Input :
      ```
      ["a","a","b","c","b","b"]
      ```
  - Output :
      ```
      "b"
      ```
- **Example 2**:
  - Input :
      ```
      ["a","a","b","c","b","b"]
      ```
  - Output :
      ```
      "b"
      ```
- **Example 3**:
  - Input :
      ```
      ["b","a","c","a","c","b"]
      ```
  - Output :
      ```
      "b"
      ```
