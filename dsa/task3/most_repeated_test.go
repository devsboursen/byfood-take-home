package task3

import (
	"strings"
	"testing"
)

func TestMostRepeated1(t *testing.T) {
	want := "b"

	got := MostRepeated([]string{"a", "a", "b", "c", "b", "b"})

	if strings.Compare(want, got) != 0 {
		t.Fatalf("got %s, wanted %s \n", got, want)
	}
}

func TestMostRepeated2(t *testing.T) {
	want := "b"

	got := MostRepeated([]string{"b", "a", "c", "a", "c", "b"})

	if strings.Compare(want, got) != 0 {
		t.Fatalf("got %s, wanted %s \n", got, want)
	}
}

func TestMostRepeated3(t *testing.T) {
	want := "red"

	got := MostRepeated([]string{"apple", "pie", "apple", "red", "red", "red"})

	if strings.Compare(want, got) != 0 {
		t.Fatalf("got %s, wanted %s \n", got, want)
	}
}

func TestMostRepeated4(t *testing.T) {
	want := ""

	got := MostRepeated([]string{})

	if strings.Compare(want, got) != 0 {
		t.Fatalf("got %s, wanted %s \n", got, want)
	}
}
