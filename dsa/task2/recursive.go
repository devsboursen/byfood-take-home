package task2

import (
	"fmt"
)

// Recursive returns a string containing some numbers separated by return character "/n".
// The numbers are the successive integer division of the input number over 2.
// The recursive division stops when absolute value of the input is less than 2.
func Recursive(number int) string {
	absNumber := abs(number)

	if absNumber == 2 {
		return "2"
	}

	if absNumber < 2 {
		return ""
	}

	return fmt.Sprintf("%s\n%d", Recursive(number/2), number)
}

func abs(number int) int {
	if number >= 0 {
		return number
	}
	return -number
}
