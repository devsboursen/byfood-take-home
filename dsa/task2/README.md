# Task 2

## Task description:

> Write a recursive function which takes one integer parameter.
Please bear in mind that finding the algorithm needed to generate the output below is the main point of the question. Please do not ask which algorithm to use.

## Examples:

- **Example 1**:
  - Input :
      ```
      9
      ```
  - Output :
      ```
      2
      4
      9
      ```
