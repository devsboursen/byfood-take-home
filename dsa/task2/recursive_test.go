package task2

import (
	"strings"
	"testing"
)

func TestRecursive(t *testing.T) {
	want := "2\n4\n9"

	got := Recursive(9)

	if strings.Compare(want, got) != 0 {
		t.Fatalf("got %s, wanted %s \n", got, want)
	}
}

func TestRecursive1(t *testing.T) {
	want := ""

	got := Recursive(0)

	if strings.Compare(want, got) != 0 {
		t.Fatalf("got %s, wanted %s \n", got, want)
	}
}
