package main

import (
	"dsa/task1"
	"dsa/task2"
	"dsa/task3"
	"fmt"
)

func main() {
	fmt.Println("Testing task 1")
	fmt.Println("--------------")
	input1 := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}
	output1 := task1.SortWords([]string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"})
	fmt.Printf("The result of calling SortWords on \n%+v \n is \n%+v \n", input1, output1)
	fmt.Println("______________________________")
	fmt.Println("")

	fmt.Println("Testing task 2")
	fmt.Println("--------------")
	input2 := 9
	output2 := task2.Recursive(input2)
	fmt.Printf("The result of calling Recursive on \n%d \n is \n%s \n", input2, output2)
	fmt.Println("______________________________")
	fmt.Println("")

	fmt.Println("Testing task 3")
	fmt.Println("--------------")
	input3 := []string{"apple", "pie", "apple", "red", "red", "red"}
	output3 := task3.MostRepeated(input3)
	fmt.Printf("The result of calling MostRepeated on \n%+v \n is \n%+v \n", input3, output3)
	fmt.Println("______________________________")
	fmt.Println("")

}
