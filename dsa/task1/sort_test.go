package task1

import (
	"reflect"
	"testing"
)


func TestSortWords1(t *testing.T) {
	want := []string{"abcd", "abc", "ab", "a", "a"}

	got := SortWords([]string{"abcd", "a", "abc", "a", "ab"})

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("got %+v, wanted %+v", got, want)
	}
}

func TestSortWords2(t *testing.T) {
	want := []string{"aaabcd", "aab", "addd", "ab", "a", "b"}

	got := SortWords([]string{"addd", "a", "aab", "aaabcd", "b", "ab"})

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("got %+v, wanted %+v", got, want)
	}
}

func TestSortWords3(t *testing.T) {
	want := []string{"aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd",
		"fdz", "ef", "kf", "zc", "l"}

	got := SortWords([]string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"})

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("got %+v, wanted %+v", got, want)
	}
}

func TestSortWords4(t *testing.T) {
	want := []string{}

	got := SortWords([]string{})

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("got %+v, wanted %+v", got, want)
	}
}
