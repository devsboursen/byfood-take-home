# Task 1

## Task description:

> Write a function that sorts a bunch of words by the number of character `a`'s within the word (decreasing order). If some words contain the same amount of character `a`'s then you need to sort
those words by their lengths.

## Examples:

- **Example 1**:
  - Input :
      ```
      ["abcd", "a", "abc", "a", "ab"]
      ```
  - Output :
      ```
      ["abcd", "abc", "ab", "a", "a"]
      ```
- **Example 2**:
  - Input :
      ```
      ["addd", "a", "aab", "aaabcd", "b", "ab"]
      ```
  - Output :
      ```
      ["aaabcd", "aab","addd", "ab", "a", "b"]
      ```
- **Example 3**:
  - Input :
      ```
      ["aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"]
      ```
  - Output :
      ```
      ["aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd",
      "fdz", "ef", "kf", "zc", "l"]
      ```
