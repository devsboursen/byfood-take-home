package task1

import (
	"sort"
	"strings"
)

// SortWords sorts the given slice of strings in-place in decreasing order with the following rules:
// 1) The more number of "a" character the bigger the string (it will come first).
// 2) If two strings have the same number of "a", the biggest on is the one with the largest length.
func SortWords(words []string) []string {
	const char = "a"

	sort.Slice(words, func(p, q int) bool {
		pCount := strings.Count(words[p], char)
		qCount := strings.Count(words[q], char)

		if pCount < qCount {
			return false
		}

		if pCount > qCount {
			return true
		}

		return len(words[p]) > len(words[q])
	})

	return words
}
